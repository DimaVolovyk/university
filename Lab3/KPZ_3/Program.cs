﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_3
{
    class Program
    {
        private static Dictionary<string, UnitType> unitTypesTable = new Dictionary<string, UnitType>()
        {
            { "Boss", UnitType.BossUnit },
            { "Regualr", UnitType.RegularUnit },
            { "Strong", UnitType.StrongUnit }
        };

        static void Main(string[] args)
        {
            var units = UnitTableMock.Units;

            Console.WriteLine("All units:");
            foreach (var item in from i in units select i)
            {
                Console.WriteLine(string.Format("->Unit type name: {0}, damage: {1}, health: {2}, position {3}",
                    item.UnitType.TypeName, item.UnitType.Damage, item.UnitType.Health, item.Position));
            }

            Position position = new Position() { X = 5, Y = 5 };
            Console.WriteLine($"\nUnits near position {position}:");
            foreach (var item in from i in units where Position.Distance(position, i.Position) <= 2 select i) 
            {
                Console.WriteLine($"->{item.Position} {item.UnitType.TypeName}");
            }

            Console.WriteLine("\nUnits grouped by type:");
            foreach (var item in from i in units
                                 group i by i.UnitType into g
                                 select new { UnitType = g.Key, Units = g.ToArray() })
            {
                Console.WriteLine($"->Type: {item.UnitType.TypeName}, count: {item.Units.Length}");
                foreach (var unit in item.Units)
                {
                    Console.WriteLine($"\t{unit.Position}");
                }
            }

            Console.WriteLine("\nUnits sorted by petname:");
            units.Sort(new CompareUnitByPetName());
            units.ForEach(unit => Console.WriteLine($"->{unit}"));

            unitTypesTable.Add("Custom", new UnitType() { Damage = 200, Health = 200, TypeName = "Custom" });
            Console.WriteLine("\nDictionary items:");
            Console.WriteLine(unitTypesTable.ToString<string, UnitType>());

            Queue<Unit> unitsQueue = new Queue<Unit>();
            foreach (var item in units)
            {
                unitsQueue.Enqueue(item);
            }

            Console.WriteLine("\nFirst element at queue:");
            Console.WriteLine(unitsQueue.Dequeue());

            Console.WriteLine($"\nAre element at position {position}: {unitsQueue.Any(i => i.Position == position)}");
            position.X = 3;
            Console.WriteLine($"\nAre element at position {position}: {unitsQueue.Any(i => i.Position == position)}");

            Console.WriteLine($"\nQueue ordered by petname descending:");
            foreach (var item in from i in unitsQueue orderby i.PetName descending select i)
            {
                Console.WriteLine($"->{item}");
            }

            Console.ReadKey();
        }
    }

    class CompareUnitByPetName : IComparer<Unit>
    {
        public int Compare(Unit x, Unit y)
        {
            return x.PetName.CompareTo(y.PetName);
        }
    }

    class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public override string ToString()
        {
            return $"({X}; {Y})";
        }

        public override bool Equals(object obj)
        {
            Position pos = obj as Position;
            return X == pos.X && Y == pos.Y;
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public static bool operator == (Position p1, Position p2)
        {
            return p1.Equals(p2);
        }

        public static bool operator !=(Position p1, Position p2)
        {
            return !p1.Equals(p2);
        }

        public static int Distance(Position p1, Position p2)
        {
            return (int)Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
    }

    class Unit
    {
        public string PetName { get; private set; }
        public UnitType UnitType { get; private set; }
        public Position Position { get; private set; }

        public Unit(UnitType type, Position position, string petName)
        {
            UnitType = type;
            Position = position;
            PetName = petName;
        }

        public override string ToString()
        {
            return $"PetName: {PetName}, type: {UnitType.TypeName}, position: {Position}";
        }
    }

    class UnitType
    {
        public  string TypeName { get; set; }
        public int Health { get; set; }
        public int Damage { get; set; }

        public override bool Equals(object obj)
        {
            return TypeName == ((UnitType)obj).TypeName;
        }

        public static bool operator == (UnitType type1, UnitType type2)
        {
            return type1.Equals(type2);
        }

        public static bool operator !=(UnitType type1, UnitType type2)
        {
            return !(type1 == type2);
        }

        public override int GetHashCode()
        {
            return 1;
        }

        public int GetHashCode(UnitType obj)
        {
            throw new NotImplementedException();
        }

        public override string ToString()
        {
            return $"TypeName: {TypeName}, Damage: {Damage}, Health: {Health}";
        }

        public static UnitType RegularUnit
        {
            get
            {
                return new UnitType() { TypeName = "Regular", Health = 100, Damage = 10 };
            }
        }

        public static UnitType StrongUnit
        {
            get
            {
                return new UnitType() { TypeName = "Strong", Health = 150, Damage = 20 };
            }
        }

        public static UnitType BossUnit
        {
            get
            {
                return new UnitType() { TypeName = "Boss", Health = 1000, Damage = 50 };
            }
        }
    }

    class UnitTableMock
    {
        public static List<Unit> Units
        {
            get
            {
                return new List<Unit>()
                {
                    new Unit(UnitType.RegularUnit, new Position(){ X = 1, Y = 2 }, "name1"),
                    new Unit(UnitType.StrongUnit, new Position(){ X = 3, Y = 5 }, "name2"),
                    new Unit(UnitType.RegularUnit, new Position(){ X = 1, Y = 4 }, "name3"),
                    new Unit(UnitType.BossUnit, new Position(){ X = 6, Y = 6 }, "Jonny"),
                    new Unit(UnitType.StrongUnit, new Position(){ X = 7, Y = 2 }, "name4"),
                    new Unit(UnitType.RegularUnit, new Position(){ X = 14, Y = 2 }, "name5"),
                    new Unit(UnitType.RegularUnit, new Position(){ X = 5, Y = 23 }, "name6"),
                    new Unit(UnitType.StrongUnit, new Position(){ X = 0, Y = 2 }, "name7")
                };
            }
        }
    }

    static class DictionaryExtension
    {
        public static string ToString<T1, T2>(this Dictionary<T1, T2> dictionary)
        {
            string result = "";
            foreach (var item in dictionary.Keys)
            {
                result += $"Key: {item}, Value: {dictionary[item]}\n";
            }
            return result;
        }
    } 
}
