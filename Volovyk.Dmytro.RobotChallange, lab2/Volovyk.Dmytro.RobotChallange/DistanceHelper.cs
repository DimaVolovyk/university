﻿using System;
using System.Linq;
using Robot.Common;

namespace Volovyk.Dmytro.RobotChallange
{
    public class DistanceHelper
    {
        public static int GetDistance(Position p1, Position p2)
        {
            return Min2D(p1.X, p2.X) + Min2D(p1.Y, p2.Y);
        }

        public static Position Add(Position p1, Position p2)
        {
            return new Position(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static Position SimetricY(Position position)
        {
            if (position.X > 50)
            {
                return new Position(position.X - 100, position.Y);
            }
            else
            {
                return new Position(position.X + 100, position.Y);
            }
        }

        public static Position SimetricX(Position position)
        {
            if (position.Y > 50)
            {
                return new Position(position.X, position.Y - 100);
            }
            else
            {
                return new Position(position.X, position.Y + 100);
            }
        }

        public static Position Sub(Position p1, Position p2)
        {
            return new Position(p2.X - p1.X, p2.Y - p1.Y);
        }

        private static int Min2D(int x1, int x2)
        {
            int[] nums =
            {
                (int) Math.Pow(x1 - x2, 2),
                (int) Math.Pow(x1 - x2 + 100, 2),
                (int) Math.Pow(x1 - x2 - 100, 2)
            };
            return nums.Min();
        }
    }
}
