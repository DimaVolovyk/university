﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Reflection;


using Robot.Common;
using Bot = Robot.Common.Robot;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;

namespace Volovyk.Dmytro.RobotChallange
{
    public class VolovykAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
            get
            {
                return "Volovyk Dmytro";
            }
        }

        public string Description
        {
            get
            {
                return "";
            }
        }

        public readonly int maxDistanceToStation = 15;
        public readonly int minCreatingEnergy = 150;
        public readonly int energyStationRadius = 3;
        public readonly int maxRobotCount = 100;

        private Owner me = null;
        private List<int> myRobots = null;
        private Dictionary<int, Position[]> tasks = null;

        private bool isFirstStep = true;

        private int lastRobotsCount = 0;
        public delegate void RobotSpawnHandler(RobotSpawnEventArgs e);
        public event RobotSpawnHandler OnRobotSpawn;
        public class RobotSpawnEventArgs : EventArgs
        {
            public List<Bot> Robots { get; set; }

            public RobotSpawnEventArgs(List<Bot> robots)
            {
                Robots = robots;
            }
        }
        
        public RobotCommand DoStep(IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            try
            {
                if (isFirstStep)
                {
                    Init(robots, robotToMoveIndex);
                    isFirstStep = false;
                }

                CheckRobotSpawn(robots);

                RegisterNewRobots(robots);

                //Creating new robot
                if (myRobots.Count < maxRobotCount && robots[robotToMoveIndex].Energy > minCreatingEnergy)
                {
                    Bot robot = new Bot()
                    {
                        Position = map.FindFreeCell(robots[robotToMoveIndex].Position, robots),
                        Energy = 100
                    };

                    var station = GetFreeStationsByDistance(robots, robotToMoveIndex, map).First();

                    if (DistanceHelper.GetDistance(station.Position, robot.Position) < Math.Pow(maxDistanceToStation, 2))
                    {
                        return new CreateNewRobotCommand();
                    }
                }

                //Moving
                if (tasks[robotToMoveIndex] == null)
                {
                    var stations = GetStationsByDistance(robots, robotToMoveIndex, map);

                    foreach (var station in stations)
                    {
                        if (!tasks.Any(p => p.Value != null
                        && p.Key != robotToMoveIndex
                        //&& robots[p.Key].Position != station.Position
                        && p.Value[0] == station.Position))
                        {
                            tasks[robotToMoveIndex] = new Position[2];
                            tasks[robotToMoveIndex][0] = station.Position;
                            var pos = GetNearestPosition(tasks[robotToMoveIndex][0], robots, robotToMoveIndex, map);
                            tasks[robotToMoveIndex][1] = pos;
                            Position newPosition = PathFinder.Next(robots[robotToMoveIndex], pos);

                            if (newPosition == robots[robotToMoveIndex].Position)
                            {
                                return new CollectEnergyCommand();
                            }

                            return new MoveCommand() { NewPosition = newPosition };
                        }
                    }
                }
                //Collecting
                else
                {
                    if (tasks[robotToMoveIndex][1] == robots[robotToMoveIndex].Position)
                    {
                        return new CollectEnergyCommand();
                    }

                    Position newPosition = PathFinder.Next(robots[robotToMoveIndex], tasks[robotToMoveIndex][1]);
                    return new MoveCommand() { NewPosition = newPosition };
                }
            }
            catch (PathNotFoundException)
            {
                return new CollectEnergyCommand();
            }

            throw new Exception("ACTION NOT CHOOSED");
        }

        public List<EnergyStation> GetStationsByDistance(IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            return (from s in map.Stations
                    //where IsCellFree(s.Position, robots, robotToMoveIndex, map)
                    orderby DistanceHelper.GetDistance(robots[robotToMoveIndex].Position, s.Position)
                    select s).ToList();
        }

        private List<EnergyStation> GetFreeStationsByDistance(IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            return (from s in map.Stations
                    where IsConquared(robots, robotToMoveIndex, map, s.Position)
                    orderby DistanceHelper.GetDistance(robots[robotToMoveIndex].Position, s.Position)
                    select s).ToList();
        }

        public Position GetNearestPosition(Position station, IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            List<Position> positions = new List<Position>();

            for (int i = -energyStationRadius; i <= energyStationRadius ; i++)
            {
                for (int j = -energyStationRadius; j <= energyStationRadius; j++)
                {
                    Position pos = new Position(station.X - i, station.Y - j);
                    if (IsValid(pos))
                    {
                        positions.Add(pos);
                    }
                }
            }

            return positions.OrderBy
                (p => DistanceHelper.GetDistance(robots[robotToMoveIndex].Position, p)).First();
        }

        private bool IsValid(Position pos)
        {
            if (pos.X <= 100 && pos.X >= 0 && pos.Y <= 100 && pos.Y >= 0) return true;
            else return false;
        }

        private List<Position> GetRefillingCellsByDistance(IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            List<Position> refCells = new List<Position>();

            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < 100; j++)
                {
                    Position cell = new Position() { X = i, Y = j };
                    if (map.GetNearbyResources(cell, energyStationRadius).Count > 0
                        && !IsConquared(robots, robotToMoveIndex, map, cell))
                    {
                        refCells.Add(cell);
                    }
                }
            }

            return refCells;
        }

        public bool IsConquared(IList<Bot> robots, int robotToMoveIndex, Map map, Position position)
        { 
            return !tasks.Any(p => p.Value != null
                    && p.Key != robotToMoveIndex
                    //&& robots[p.Key].Position != station.Position
                    && p.Value[0] == position);
        }

        private void Init(IList<Bot> robots, int robotToMoveIndex)
        {
            Logger.OnLogMessage += Logger_OnLogMessage;
            Logger.OnLogRound += Logger_OnLogRound;
            me = robots[robotToMoveIndex].Owner;
            lastRobotsCount = robots.Count;
            myRobots = new List<int>();
            tasks = new Dictionary<int, Position[]>();
            for (int i = 0; i < robots.Count; i++)
            {
                if(robots[i].Owner == me)
                {
                    myRobots.Add(i);
                    tasks.Add(i, null);
                }
            }
            Task task = new Task(() => Application.Run(new Window(this)));
            task.Start();
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            File.AppendAllText("log.txt", $"End of round {e.Number}\r\n");
        }
        
        private void Logger_OnLogMessage(object sender, LogEventArgs e)
        {
            File.AppendAllText($"log.txt", $"Message: {e.Message}\r\n");
        }

        private void CheckRobotSpawn(IList<Bot> robots)
        {
            if (lastRobotsCount < robots.Count && OnRobotSpawn != null)
            {
                OnRobotSpawn(new RobotSpawnEventArgs(new List<Bot>(robots.Reverse().Take(robots.Count - lastRobotsCount))));
                lastRobotsCount = robots.Count;
            }
        }

        private void RegisterNewRobots(IList<Bot> robots)
        {
            for (int i = myRobots.Last() + 1; i < robots.Count; i++)
            {
                if (robots[i].Owner.Name == me.Name)
                {
                    myRobots.Add(i);
                    tasks.Add(i, null);
                }
            }
        }

        private bool IsCellFree(Position cell, IList<Bot> robots, int robotToMoveIndex, Map map)
        {
            foreach (var robot in robots)
            {
                if (robot != robots[robotToMoveIndex])
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
    }
}
