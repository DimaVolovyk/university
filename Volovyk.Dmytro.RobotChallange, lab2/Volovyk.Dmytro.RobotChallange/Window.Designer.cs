﻿

using System.Windows.Forms;
using static Volovyk.Dmytro.RobotChallange.VolovykAlgorithm;

namespace Volovyk.Dmytro.RobotChallange
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        public Window(VolovykAlgorithm algorithm)
        {
            InitializeComponent();
            algorithm.OnRobotSpawn += this.Algorithm_OnRobotSpawn;
            this.algorithm = algorithm;
        }

        private void Algorithm_OnRobotSpawn(RobotSpawnEventArgs e)
        {
            Invoke(new MethodInvoker(delegate
                {
                    foreach (var item in e.Robots)
                    {
                        listBox1.Items.Add($"New robot at position {item.Position}. Owner {item.Owner.Name}");
                    }
                }
            ));
        }

        protected override void OnFormClosed(FormClosedEventArgs e)
        {
            base.OnFormClosed(e);
            algorithm.OnRobotSpawn -= this.Algorithm_OnRobotSpawn;
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(13, 13);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(775, 420);
            this.listBox1.TabIndex = 0;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.listBox1);
            this.Name = "Window";
            this.Text = "Window";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private VolovykAlgorithm algorithm;
    }
}