﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Volovyk.Dmytro.RobotChallange
{
    class PathFinder
    {
        public static List<Position> FindPath(Robot.Common.Robot robot, Position destination, int stepCount)
        {
            List<Position> path = new List<Position>();

            Position direction = DistanceHelper.Sub(robot.Position, destination);
            direction.X /= stepCount;
            direction.Y /= stepCount;

            Position prev = robot.Position;

            for (int i = 0; i < stepCount; i++)
            {
                if (i == stepCount - 1)
                {
                    path.Add(destination);
                }
                else
                {
                    path.Add(DistanceHelper.Add(prev, direction));
                }
                prev = path.Last();
            }

            if (path.Count == 0)
            {
                throw new PathNotFoundException(robot, destination);
            }

            return path;
        }

        public static List<Position> FindPath(Robot.Common.Robot robot, Position destination)
        {
            List<Position> path = new List<Position>();

            for (int i = 1; i <= 100; i++)
            {
                CreatePath(i, robot.Position, destination, path);
                
                if (GetCost(robot, path) <= robot.Energy)
                {
                    break;
                }
                else
                {
                    path.Clear();
                }
            }

            if (path.Count == 0)
            {
                throw new PathNotFoundException(robot, destination);
            }

            return path;
        }

        public static Position Next(Robot.Common.Robot robot, Position destination)
        {
            return FindPath(robot, destination).First();
        }

        public static int GetCost(Robot.Common.Robot robot, List<Position> path)
        {
            int cost = 0;
            Position prev = robot.Position;

            for (int i = 0; i < path.Count; i++)
            {
                cost += DistanceHelper.GetDistance(prev, path[i]);
                prev = path[i];
            }

            return cost;
        }

        private static void CreatePath(int stepCount, Position from, Position to, List<Position> path)
        {
            Position direction = DistanceHelper.Sub(from, to);

            if(direction.PowdedLenght() > DistanceHelper.GetDistance(from, to))
            {
                if (direction.X > 50)
                {
                    direction = DistanceHelper.Sub(from, DistanceHelper.SimetricY(to));
                }
                else if(direction.Y > 50)
                {
                    direction = DistanceHelper.Sub(from, DistanceHelper.SimetricX(to));
                }
            }

            direction.X /= stepCount;
            direction.Y /= stepCount;
            
            Position prev = from;

            for (int i = 0; i < stepCount; i++)
            {
                if (i == stepCount - 1)
                {
                    path.Add(to);
                }
                else
                {
                    Position p = DistanceHelper.Add(prev, direction);
                    if (p.X < 0)
                    {
                        p = DistanceHelper.SimetricY(p);
                    }
                    if (p.Y < 0)
                    {
                        p = DistanceHelper.SimetricX(p);
                    }
                    path.Add(p);
                }
                prev = path.Last();
            }
        }
    }
    
    [Serializable]
    public class PathNotFoundException : Exception
    {
        public Position Destination { get; set; }
        public Robot.Common.Robot Robot { get; set; }

        public PathNotFoundException(Robot.Common.Robot robot, Position destination) 
            : base(String.Format("Path from {0} to {1} not found", robot.Position, destination))
        {
            Robot = robot;
            Destination = destination;
        }
        public PathNotFoundException() { }
        public PathNotFoundException(string message) : base(message) { }
        public PathNotFoundException(string message, Exception inner) : base(message, inner) { }
        protected PathNotFoundException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    static class PositionExtension
    {
        public static int PowdedLenght(this Position position)
        {
            return (int)(Math.Pow(position.X, 2) + Math.Pow(position.Y, 2));
        }
    }
}
