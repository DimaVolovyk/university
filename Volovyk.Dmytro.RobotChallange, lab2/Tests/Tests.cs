﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using Volovyk.Dmytro.RobotChallange;
using System.Diagnostics;

namespace Tests
{
    [TestClass]
    public class Tests
    {
        private VolovykAlgorithm algorithm = new VolovykAlgorithm();

        [TestMethod]
        public void TestChoosingStation()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100), MinPozition = new Position(0, 0) };
            map.Stations.Add(new EnergyStation() { Position = new Position(5, 5) });
            map.Stations.Add(new EnergyStation() { Position = new Position(50, 30) });
            var s = algorithm.GetStationsByDistance(
                new List<Robot.Common.Robot>()
                { new Robot.Common.Robot() { Position = new Position(0, 0), Energy = 100 } },
                0,
                map
                );
            Assert.AreEqual(s[0].Position, new Position(5, 5));
        }

        [TestMethod]
        public void TestLongRangePathing()
        {
            Map map = new Map();
            var robot = new Robot.Common.Robot() { Position = new Position(10, 50), Energy = 100 };
            map.Stations.Add(new EnergyStation() { Position = new Position(90, 50) });
            for (int i = 0; i < 100; i++)
            {
                var cmd = algorithm.DoStep(
                    new List<Robot.Common.Robot>()
                    { robot },
                    0,
                    map
                    );
                if (cmd is CollectEnergyCommand) break;
                var moveCommand = cmd as MoveCommand;
                robot.Position = moveCommand.NewPosition;
                robot.Energy -= 1;
            }
           
            Assert.AreEqual(robot.Position, new Position(93, 50));
        }

        [TestMethod]
        public void TestRobotCreating()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            var robot = new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = algorithm.minCreatingEnergy + 1,
                Owner = new Owner() { Name = "Volovyk Dmytro" }
            };
            map.Stations.Add(new EnergyStation() { Position = new Position(0, 1) });
            map.Stations.Add(new EnergyStation() { Position = new Position(0, algorithm.maxDistanceToStation) });
            var cmd = algorithm.DoStep(
                   new List<Robot.Common.Robot>()
                   { robot },
                   0,
                   map
                   );
            Assert.IsInstanceOfType(cmd, typeof(CreateNewRobotCommand));
        }

        [TestMethod]
        public void TestFindingNearestPositionToStation()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 10) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(0, 10)
            };
            var pos = 
                algorithm.GetNearestPosition(new Position(10, 10), new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.AreEqual(pos, new Position(10 - algorithm.energyStationRadius, 10));
        }

        [TestMethod]
        public void TestPathingThrowMapYBound()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(90, 10) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(0, 10),
                Energy = 100
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(MoveCommand));
            Assert.AreEqual((cmd as MoveCommand).NewPosition, new Position(93, 10));
        }

        [TestMethod]
        public void TestPathingThrowMapXBound()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(0, 90) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(MoveCommand));
            Assert.AreEqual((cmd as MoveCommand).NewPosition, new Position(0, 93));
        }

        [TestMethod]
        public void TestCollectingEnergy()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 10) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(8, 8),
                Energy = 100
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(CollectEnergyCommand));
        }

        [TestMethod]
        public void TestChoosingAction1()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 10) });
            map.Stations.Add(new EnergyStation() { Position = new Position(90, 10) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(8, 8),
                Energy = algorithm.minCreatingEnergy + 1
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(CreateNewRobotCommand));
        }

        [TestMethod]
        public void TestChoosingAction2()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(10, 10) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(8, 8),
                Energy = algorithm.minCreatingEnergy - 1
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(CollectEnergyCommand));
        }

        [TestMethod]
        public void TestChoosingAction3()
        {
            Map map = new Map() { MaxPozition = new Position(100, 100) };
            map.Stations.Add(new EnergyStation() { Position = new Position(50, 50) });
            Robot.Common.Robot robot = new Robot.Common.Robot()
            {
                Position = new Position(8, 8),
                Energy = 100
            };
            var cmd = algorithm.DoStep(new List<Robot.Common.Robot>() { robot }, 0, map);
            Assert.IsInstanceOfType(cmd, typeof(MoveCommand));
        }
    }
}
