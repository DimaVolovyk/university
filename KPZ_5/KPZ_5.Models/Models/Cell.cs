﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace KPZ_5.Models
{
    [Serializable]
    class Cell
    {
        public int X { get; set; }
        public int Y { get; set; }

        private CellType _cellType;
        public CellType CellType
        {
            get
            {
                return _cellType;
            }
            set
            {
                _cellType = value;
                switch (CellType)
                {
                    case CellType.Desert: Texture = _desertTexture; break;
                    case CellType.Field: Texture = _fieldTexture; break;
                    case CellType.Snow: Texture = _snowTexture; break;
                    case CellType.Water: Texture = _waterTexture; break;
                }
            }

        }
        
        [OnDeserialized()]
        internal void OnDeserialized(StreamingContext context)
        {
            switch (CellType)
            {
                case CellType.Desert: Texture = _desertTexture; break;
                case CellType.Field: Texture = _fieldTexture; break;
                case CellType.Snow: Texture = _snowTexture; break;
                case CellType.Water: Texture = _waterTexture; break;
            }
        }

        [NonSerialized]
        private BitmapImage _texture;
        public BitmapImage Texture { get => _texture; set => _texture = value; }

        public Cell(int x, int y, CellType type)
        {
            X = x;
            Y = y;
            CellType = type;
        }

        private static BitmapImage _desertTexture = new BitmapImage(new Uri(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\DesertTexture.jpg"));
       
        private static BitmapImage _fieldTexture = new BitmapImage(new Uri(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\FieldTexture.jpg"));
        
        private static BitmapImage _snowTexture = new BitmapImage(new Uri(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\SnowTexture.jpg"));
       
        private static BitmapImage _waterTexture = new BitmapImage(new Uri(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\WaterTexture.jpg"));
    }

    enum CellType
    {
        Unknown,
        Desert,
        Field,
        Water,
        Snow
    }
}
