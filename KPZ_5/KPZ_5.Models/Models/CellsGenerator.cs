﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_5.Models
{
    class CellsGenerator
    {
        private int startPointsCount = 4;
        private double _voronoiDistance = 1;

        private Random _random = new Random(DateTime.Now.Millisecond);

        public Cell[,] GenerateRandomCells(int xLenght, int yLenght)
        {
            Cell[,] cells = new Cell[xLenght, yLenght];

            List<Cell> controls = new List<Cell>();
            for (int i = 0; i < startPointsCount; i++)
            {
                controls.Add(new Cell(_random.Next(0, xLenght + 1), _random.Next(0, yLenght + 1), CellType.Field));
                controls.Add(new Cell(_random.Next(0, xLenght + 1), _random.Next(0, yLenght + 1), CellType.Desert));
                controls.Add(new Cell(_random.Next(0, xLenght + 1), _random.Next(0, yLenght + 1), CellType.Snow));

            }

            for (int i = 0; i < xLenght; i++)
            {
                for (int j = 0; j < yLenght; j++)
                {
                    cells[i, j] = new Cell(i, j, CellType.Unknown);
                }
            }

            while (cells.Any(i => i.CellType == CellType.Unknown))
            {
                List<Cell> toBeAdded = new List<Cell>();
                for (int i = 0; i < xLenght; i++)
                {
                    for (int j = 0; j < yLenght; j++)
                    {
                        if (cells[i, j].CellType == CellType.Unknown)
                        {
                            var closest = (from item in controls orderby Distance(item, cells[i, j]) select item).First();
                            if (Distance(closest, cells[i, j]) <= _voronoiDistance)
                            {
                                toBeAdded.Add(cells[i, j]);
                                cells[i, j].CellType = closest.CellType;
                            }
                        }
                    }
                }
                controls.AddRange(toBeAdded);
            }

            return cells;
        }

        private double Distance(Cell p1, Cell p2)
        {
            return Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2));
        }
    }

    static class MultiDimensionalExtension
    {
        public static bool Any(this Cell[,] cells, Func<Cell, bool> func)
        {
            for (int i = 0; i < cells.GetLength(0); i++)
            {
                for (int j = 0; j < cells.GetLength(1); j++)
                {
                    if (func(cells[i, j]))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
