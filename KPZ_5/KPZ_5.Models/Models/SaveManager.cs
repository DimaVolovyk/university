﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_5.Models
{
    class SaveManager
    {
        public static Map Load(string path)
        {
            FileStream stream = File.Open(path, FileMode.OpenOrCreate);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            var map = binaryFormatter.Deserialize(stream) as Map;
            return map;
        }

        public static void Save(string path, Map instanceToSave)
        {
            FileStream stream = File.Open(path, FileMode.OpenOrCreate);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            stream.Seek(0, SeekOrigin.Begin);
            binaryFormatter.Serialize(stream, instanceToSave);
        }
    }
}
