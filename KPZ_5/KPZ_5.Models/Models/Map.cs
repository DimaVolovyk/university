﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_5.Models
{
    [Serializable]
    class Map
    {
        public Cell[,] Cells { get; }

        public Map(int xLenght, int yLenght)
        {
            CellsGenerator generator = new CellsGenerator();
            Cells = generator.GenerateRandomCells(xLenght, yLenght);
        }
    }
}
