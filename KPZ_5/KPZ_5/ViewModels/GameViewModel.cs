﻿using KPZ_5.Models;
using KPZ_5.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using ContextMenu = KPZ_5.Views.ContextMenu;

namespace KPZ_5.ViewModels
{
    class GameViewModel
    {
        public List<Cell> Cells { get; set; }
        private Map Map { get; set; }
        private StackPanel _menu = new ContextMenu();
        private StackPanel _save = new SaveWindow();
        public Page Page { get; set; }

        public Command OpenContexMenuCommand { get; set; }
        public Command CloseContextMenuCommand { get; set; }
        public Command OpenSaveWindowCommand { get; private set; }
        public Command SaveGameCommand { get; private set; }
        public Command OpenMainMenuCommand { get; set; }

        public GameViewModel() : this(null){}

        public GameViewModel(Map map = null)
        {
            OpenContexMenuCommand = new Command(() =>
            {
                foreach (UIElement item in ((Grid)Page.Content).Children)
                {
                    item.IsEnabled = false;
                }
                ((Grid)Page.Content).Children.Add(_menu);
            });

            OpenMainMenuCommand = new Command(() =>
            {
                var mainMenu = new MainMenu();
                mainMenu.DataContext = new MainMenuViewModel() { Page = mainMenu };
                ((Window)Page.Parent).Content = mainMenu;
            });

            CloseContextMenuCommand = new Command(() =>
            {
                ((Grid)Page.Content).Children.Remove(_menu);
                foreach (UIElement item in ((Grid)Page.Content).Children)
                {
                    item.IsEnabled = true;
                }
            });

            OpenSaveWindowCommand = new Command(() =>
            {
                ((Grid)Page.Content).Children.Remove(_menu);
                ((Grid)Page.Content).Children.Add(_save);
            });

            SaveGameCommand = new Command(() =>
            {
                string name = @"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\Saves\" + ((TextBox)_save.Children[0]).Text;
                SaveManager.Save(name, Map);
                ((Grid)Page.Content).Children.Add(_menu);
                ((Grid)Page.Content).Children.Remove(_save);
            });
            if (map == null)
            {
                Map = new Map(40, 40);
            }
            else { Map = map; }
            Cells = new List<Cell>();
            var cells = Map.Cells;
            foreach (var item in cells)
            {
                Cells.Add(item);
            }
        }
    }
}
