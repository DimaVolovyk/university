﻿using KPZ_5.Models;
using KPZ_5.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace KPZ_5.ViewModels
{
    class MainMenuViewModel
    {
        public ICommand ExitCommand { get; set; } = new Command(() => App.Current.Shutdown());
        public Command NewGameCommand { get; set; }
        public Command OpenLoadGameWindowCommand { get; private set; }
        public Command LoadGameCommand { get; private set; }
        public Page Page { get; set; }
        public List<Save> Saves { get; set; }

        public MainMenuViewModel()
        {
            NewGameCommand = new Command(() =>
            {
                //var childs = (((Grid)Page.Content).Children[0] as StackPanel).Children;
                //var exitButton = childs[2] as Button;
                //(exitButton.Content as TextBlock).Text = "Loading...";
                //childs.Clear();
                //childs.Add(exitButton);
                var gamePage = new Game();
                gamePage.DataContext = new GameViewModel() { Page = gamePage };
                ((Window)Page.Parent).Content = gamePage;
            });

            OpenLoadGameWindowCommand = new Command(() =>
            {
                Saves = new List<Save>();
                string[] files = Directory.GetFiles(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\Saves");
                foreach (var item in files)
                {
                    Saves.Add(new Save(item));
                }
                var childs = ((Grid)Page.Content).Children;
                childs.Clear();
                childs.Add(new Load());
            });

            LoadGameCommand = new Command(() =>
            {
                var dataGrid = (((StackPanel)((Grid)Page.Content).Children[0]).Children[0] as DataGrid);
                var name = ((Save)dataGrid.Items[dataGrid.SelectedIndex]).Name;

                var map = SaveManager.Load(@"C:\Users\Dima\source\repos\KPZ_5\KPZ_5\bin\Debug\Saves\" + name);
                var gamePage = new Game();
                gamePage.DataContext = new GameViewModel(map) { Page = gamePage };
                ((Window)Page.Parent).Content = gamePage;
            });
        }
    }

    class Save
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }

        public Save(string path)
        {
            Name = path.Split('\\').Last();
            Date = Directory.GetCreationTime(path);
        }
    }

    class Command : ICommand
    {
        private Action _action;

        public event EventHandler CanExecuteChanged;

        public Command(Action action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _action?.Invoke();
        }
    }
}
