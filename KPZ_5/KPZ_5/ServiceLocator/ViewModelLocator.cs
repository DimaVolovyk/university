﻿using KPZ_5.Models;
using KPZ_5.ViewModels;
using Ninject;
using Ninject.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_5.ServiceLocator
{
    class ViewModelLocator
    {
        public GameViewModel GameViewModel => App.Ninjectkernel.Get<GameViewModel>
            (new ConstructorArgument("map", App.Ninjectkernel.Get<Map>("Big")));
        public MainMenuViewModel MainMenuViewModel => App.Ninjectkernel.Get<MainMenuViewModel>();
    }
}
