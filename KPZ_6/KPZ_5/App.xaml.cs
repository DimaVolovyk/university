﻿using KPZ_5.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using Ninject;
using Ninject.Modules;
using KPZ_5.ViewModels;

namespace KPZ_5
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static StandardKernel Ninjectkernel { get; private set; }
        
        public App()
        {
            IocConfiguration configuration = new IocConfiguration();
            Ninjectkernel = new StandardKernel(configuration);
        }
    }

    public class IocConfiguration : NinjectModule
    {
        public override void Load()
        {
            Bind<GameViewModel>().ToSelf().InTransientScope();
            Bind<MainMenuViewModel>().ToSelf().InSingletonScope();
            Bind<Map>().ToConstant(new Map(20, 20)).Named("Small");
            Bind<Map>().ToConstant(new Map(40, 40)).Named("Big");
        }
    }
}