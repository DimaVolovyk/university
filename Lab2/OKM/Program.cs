﻿



using System;

namespace OKM
{
    class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server();
            server.StartListeningAsync();

            server.OnIncomingData += delegate (MessageWithCallbackEventArgs e)
             {
                 return $"Data sended to server {e.Message}\n";
             };

            server.OnIncomingData += delegate(MessageWithCallbackEventArgs e)
            {
                double angle = ConvertToRadians(Double.Parse(e.Message.Split(' ')[0]));
                string result = String.Format("sin = {0} cos = {1} tg = {2} ctg = {3}\n",
                Math.Sin(angle), Math.Cos(angle), Math.Tan(angle), 1.0 / Math.Tan(angle));
                return result;
            };

            Console.ReadKey();
        }
        
        private  static double ConvertToRadians(double angle)
        {
            return (Math.PI / 180) * angle;
        }
    }
}
