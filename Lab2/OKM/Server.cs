﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace OKM
{
    public class Server
    {
        private UdpClient server;

        public int Port { get; set; } = 7777;

        public delegate string MessageWithCallback(MessageWithCallbackEventArgs e);
        public event MessageWithCallback OnIncomingData;

        public async void StartListeningAsync()
        {
            await Task.Run(delegate { StartListening(); });
        }

        public void StartListening()
        {
            server = new UdpClient(Port);
            IPEndPoint groupEP = new IPEndPoint(IPAddress.Any, Port);

            try
            {
                Console.WriteLine("Server started");
                
                while (true)
                {
                    Console.WriteLine("Waiting for broadcast");
                    byte[] bytes = server.Receive(ref groupEP);
                    string incoming = Encoding.Unicode.GetString(bytes, 0, bytes.Length);

                    Console.WriteLine("Received broadcast from {0} :\n {1}\n",
                        groupEP.ToString(),
                        incoming);

                    string[] splited = incoming.Split(' ');

                    if (OnIncomingData != null)
                    {
                        string result = "";
                        foreach (var method in OnIncomingData.GetInvocationList())
                        {
                            result += ((MessageWithCallback)method)(new MessageWithCallbackEventArgs() { Message = incoming });
                        }
                        bytes = Encoding.Unicode.GetBytes(result);
                        server.Send(bytes, bytes.Length, new IPEndPoint(groupEP.Address, groupEP.Port));
                        Console.WriteLine("Sended to {0}:\n {1}", groupEP.ToString(), result);
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                server.Close();
            }
        }
    }

    public class MessageWithCallbackEventArgs : EventArgs
    {
        public string Message { get; set; }
    }
}