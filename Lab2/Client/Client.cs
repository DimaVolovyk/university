﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace Client
{
    public class Client
    {
        private UdpClient socket = new UdpClient();

        public void Send(string ip, int port, string message)
        {
            try
            {
                IPEndPoint point = new IPEndPoint(IPAddress.Parse(ip), port);
                byte[] bytes = Encoding.Unicode.GetBytes(message);
                socket.Send(bytes, bytes.Length, point);
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Socket error: " + e.Message);
            }

        }

        public string Receive(string ip, int port)
        {
            IPEndPoint point = new IPEndPoint(IPAddress.Parse(ip), port);
            byte[] bytes = socket.Receive(ref point);
            return Encoding.Unicode.GetString(bytes);
        }
    }
}
