﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            string input = "";
            while (input != "Exit")
            {
                input = Console.ReadLine();
                string[] splited = input.Split(' ');

                switch (splited[0])
                {
                    case "Send":
                        client.Send(splited[1], int.Parse(splited[2]), splited[3]);
                        Console.WriteLine(client.Receive(splited[1], int.Parse(splited[2])));
                        break;
                    case "Exit":
                        break;
                    default:
                        Console.WriteLine("Wrong command");
                        break;
                }
            }
        }
    }
}
