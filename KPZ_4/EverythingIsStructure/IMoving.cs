﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    interface IMoving
    {
        void Move(Vector2 position);
        void Move(double x, double y);
    }

    public struct Vector2
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Lenght => Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));

        public static void Normalize(ref Vector2 vector)
        {
            vector.X = vector.X / vector.Lenght;
            vector.Y = vector.Y / vector.Lenght;
        }

        public static void Normalize(ref Vector2 origin, out Vector2 normalized)
        {
            normalized = new Vector2() { X = origin.X / origin.Lenght, Y = origin.Y / origin.Lenght };
        }

        public static implicit operator Tuple<double, double>(Vector2 vector)
        {
            return new Tuple<double, double>(vector.X, vector.Y);
        }  

        public static explicit operator Vector2(Tuple<double,double> tuple)
        {
            return new Vector2() { X = tuple.Item1, Y = tuple.Item2 };
        }
    }

    public static class Vector2Extension
    {
        public static Vector2 Normalize(this Vector2 vector)
        {
            return new Vector2() { X = vector.X / vector.Lenght, Y = vector.Y / vector.Lenght };
        }
    }
}
