﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    struct Warrior : IFighting, IMoving, BaseStruct
    {
        private static int _instanceCount;

        private Weapon Gun { get; set; }

        public int Damage { get; }
        public string Name { get; set; }
        public int Health { get; set; }
        public Vector2 Position { get; set; }

        public WorldObjectTypes Type { get; }

        static Warrior()
        {
            _instanceCount = 0;
        }

        public Warrior(int health, int damage, Vector2 position, string name)
        {
            Health = health;
            Position = position;
            Name = name;
            this.Damage = damage;
            Type = WorldObjectTypes.Ground;
            Gun = new Weapon();
        }
        
        public void Attack(WorldObject target)
        {
            if (target.Type == WorldObjectTypes.Ground || target.Type == this.Type)
            {
                target.TakeDamage(Damage);
            }
        }

        public void Move(Vector2 position)
        {
            if (this.Type != WorldObjectTypes.Water && Type != WorldObjectTypes.Air)
            {
                Position = position;
            }
        }

        private void Die()
        {
            Console.WriteLine("It was warrior");
        }

        public void Move(double x, double y)
        {
            Move(new Vector2() { X = x, Y = y });
        }

        public void TakeDamage(int damage)
        {
            throw new NotImplementedException();
        }

        private class Weapon
        {
            public string Name { get; set; }
        }
    }
}
