﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    public struct WorldObject : BaseStruct
    {
        static int _instanceCount = 0;

        public string Name { get; set; }
        public int Health { get; set; }
        public Vector2 Position { get; set; }
        public WorldObjectTypes Type { get; }

        public WorldObject(int health, Vector2 position, string name, WorldObjectTypes type = WorldObjectTypes.Ground)
        {
            Type = type;
            Health = health;
            Position = position;
            Name = name;
            _instanceCount++;
        }

        public void TakeDamage(int damage)
        {
            if(Health > damage)
            {
                Console.WriteLine($"{Name} under attack");
                Health -= damage;
            }
            else
            {
                Die();
            }
        }

        private void Die()
        {
            Console.WriteLine($"{Name} dead");
        }
    }

    public interface BaseStruct
    {
        string Name { get; set; }
        int Health { get; set; }
        Vector2 Position { get; set; }
        WorldObjectTypes Type { get; }

        void TakeDamage(int damage);
    }

}
