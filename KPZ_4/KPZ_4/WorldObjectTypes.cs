﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    [Flags]
    public enum  WorldObjectTypes
    {
        Ground,
        Air,
        Water
    }
}
