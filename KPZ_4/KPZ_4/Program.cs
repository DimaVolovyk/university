﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;

namespace KPZ_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            object b = (object)a; //boxing
            Console.WriteLine($"a = {a}, b = {b}");
            a = 10;
            int c = (int)b; //unboxing
            Console.WriteLine($"a = {a}, b = {b}, c = {c}");
            Console.ReadKey();
            Console.Clear();

            //------------------------------------------

            object obj = new object();
            object obj2 = new object();
            Console.WriteLine("Object");
            Console.WriteLine(
                $"obj.ToString(): {obj.ToString()}\n" +
                $"obj.GetHashCode(): {obj.GetHashCode()}\n" +
                $"obj.Equal(obj2): {obj.Equals(obj2)}\n");


            Console.WriteLine("MyObject");
            object obj1 = new MyObject();
            object obj12 = new MyObject();
            Console.WriteLine(
                $"obj1.ToString(): {obj1.ToString()}\n" +
                $"obj1.GetHashCode(): {obj1.GetHashCode()}\n" +
                $"obj1.Equal(obj12): {obj1.Equals(obj12)}");

            Console.ReadKey();
            Console.Clear();
            //----------------------------------------

            int iterationCount = 10000000;

            Stopwatch timer = new Stopwatch();

            Console.WriteLine("For structures");
            timer.Start();

            for (int i = 0; i < iterationCount; i++)
            {
                Vector2 vector = new Vector2() { X = 0, Y = 0 };
                DoSmthg1(ref vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with ref parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                Vector2 vector = new Vector2() { X = 0, Y = 0 };
                DoSmthg2(out vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with out parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                Vector2 vector = new Vector2() { X = 0, Y = 0 };
                DoSmthg3(vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with usual parameter: {timer.Elapsed.TotalSeconds} seconds");


            //------------------------------------------------

            Console.WriteLine("For classes");
            timer.Restart();

            for (int i = 0; i < iterationCount; i++)
            {
                Vector2Class vector = new Vector2Class() { X = 0, Y = 0 };
                DoSmthg1(ref vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with ref parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                Vector2Class vector = new Vector2Class() { X = 0, Y = 0 };
                DoSmthg2(out vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with out parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                Vector2Class vector = new Vector2Class() { X = 0, Y = 0 };
                DoSmthg3(vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with usual parameter: {timer.Elapsed.TotalSeconds} seconds");

            //--------------------------------------------------------

            Console.WriteLine("For increased number of fields");
            timer.Restart();

            for (int i = 0; i < iterationCount; i++)
            {
                VectorN vector = new VectorN() { X = 0, Y = 0 };
                DoSmthg1(ref vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with ref parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                VectorN vector = new VectorN() { X = 0, Y = 0 };
                DoSmthg2(out vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with out parameter: {timer.Elapsed.TotalSeconds} seconds");

            timer.Restart();
            for (int i = 0; i < iterationCount; i++)
            {
                VectorN vector = new VectorN() { X = 0, Y = 0 };
                DoSmthg3(vector);
            }
            timer.Stop();
            Console.WriteLine($"Time for function with usual parameter: {timer.Elapsed.TotalSeconds} seconds");

            Console.ReadKey();

        }

        static void DoSmthg1(ref Vector2 vector)
        {
            vector = new Vector2() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg2(out Vector2 vector)
        {
            vector = new Vector2() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg3(Vector2 vector)
        {
            vector = new Vector2() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg1(ref Vector2Class vector)
        {
            vector = new Vector2Class() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg2(out Vector2Class vector)
        {
            vector = new Vector2Class() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg3(Vector2Class vector)
        {
            vector = new Vector2Class() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg1(ref VectorN vector)
        {
            vector = new VectorN() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg2(out VectorN vector)
        {
            vector = new VectorN() { X = 1000, Y = 1000 }.Normalize();
        }

        static void DoSmthg3(VectorN vector)
        {
            vector = new VectorN() { X = 1000, Y = 1000 }.Normalize();
        }
    }

    class MyObject
    {
        public override int GetHashCode()
        {
            return base.GetHashCode() % 10;
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return "It`s my object";
        }
    }

    public class Vector2Class
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Lenght => Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));

        public static void Normalize(ref Vector2Class vector)
        {
            vector.X = vector.X / vector.Lenght;
            vector.Y = vector.Y / vector.Lenght;
        }

        public static void Normalize(ref Vector2Class origin, out Vector2Class normalized)
        {
            normalized = new Vector2Class() { X = origin.X / origin.Lenght, Y = origin.Y / origin.Lenght };
        }

        public static implicit operator Tuple<double, double>(Vector2Class vector)
        {
            return new Tuple<double, double>(vector.X, vector.Y);
        }

        public static explicit operator Vector2Class(Tuple<double, double> tuple)
        {
            return new Vector2Class() { X = tuple.Item1, Y = tuple.Item2 };
        }
    }

    public class VectorN
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z1 { get; set; }
        public double Z2 { get; set; }
        public double Z3 { get; set; }
        public double Z4 { get; set; }
        public double Z5 { get; set; }
        public double Z6 { get; set; }
        public double Z7 { get; set; }
        public double Z8 { get; set; }
        public double Z9 { get; set; }
        public double Z10 { get; set; }
        public double Z11 { get; set; }
        public double Z12 { get; set; }
        public double Lenght => Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));

        public static void Normalize(ref Vector2Class vector)
        {
            vector.X = vector.X / vector.Lenght;
            vector.Y = vector.Y / vector.Lenght;
        }

        public static void Normalize(ref Vector2Class origin, out Vector2Class normalized)
        {
            normalized = new Vector2Class() { X = origin.X / origin.Lenght, Y = origin.Y / origin.Lenght };
        }

        public static implicit operator Tuple<double, double>(VectorN vector)
        {
            return new Tuple<double, double>(vector.X, vector.Y);
        }

        public static explicit operator VectorN(Tuple<double, double> tuple)
        {
            return new VectorN() { X = tuple.Item1, Y = tuple.Item2 };
        }
    }
}
