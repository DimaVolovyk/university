﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    public abstract class WorldObject
    {
        static int _instanceCount = 0;
        private string _name = "none";

        public string Name => _name;
        public int Health { get; protected set; }
        public Vector2 Position { get; protected set; }
        public WorldObjectTypes Type { get; }

        public WorldObject(int health, Vector2 position, string name, WorldObjectTypes type = WorldObjectTypes.Ground)
        {
            Type = type;
            Health = health;
            Position = position;
            _name = name;
            _instanceCount++;
        }
        
        public WorldObject()
        {
            Type = WorldObjectTypes.Ground;
            Health = 100;
            Position = new Vector2() { X = 0, Y = 0 };
            _name = "unknown";
            _instanceCount++;
        }

        public virtual void TakeDamage(int damage)
        {
            if(Health > damage)
            {
                Console.WriteLine($"{_name} under attack");
                Health -= damage;
            }
            else
            {
                Die();
            }
        }

        protected virtual void Die()
        {
            Console.WriteLine($"{_name} dead");
        }
    }
}
