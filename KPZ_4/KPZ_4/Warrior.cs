﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_4
{
    class Warrior : WorldObject, IFighting, IMoving
    {
        private static int _instanceCount;

        protected Weapon Gun { get; set; }

        public int Damage { get; }

        static Warrior()
        {
            _instanceCount = 0;
        }

        public Warrior(int health, int damage, Vector2 position, string name) : base(health, position, name)
        {
            this.Damage = damage;
        }

        public Warrior()
        {
            Damage = 100;
        }

        public void Attack(WorldObject target)
        {
            if (target.Type == WorldObjectTypes.Ground || target.Type == this.Type)
            {
                target.TakeDamage(Damage);
            }
        }

        public void Move(Vector2 position)
        {
            if (this.Type != WorldObjectTypes.Water && Type != WorldObjectTypes.Air)
            {
                Position = position;
            }
        }

        protected override void Die()
        {
            base.Die();
            Console.WriteLine("It was warrior");
        }

        public void Move(double x, double y)
        {
            Move(new Vector2() { X = x, Y = y });
        }

        protected class Weapon
        {
            public string Name { get; set; }
        }
    }
}
